var generalNumber = 100,
	textString = 'some clickable element';

var simpleModule = (function () {
	return {
		init: function (conf) {
			var rootElem = document.querySelector('#playgroundVanila'),
				ulElem = document.createElement('ul'),
				liElem = document.createElement('li'),
				clsToggler = function (e) {
					e.target.classList.toggle('fancy')
				},
				conf = conf || {},
				number = conf.howMany || generalNumber,
				text = conf.text || textString,
				withIndex = conf.withIndex;

			rootElem.appendChild(ulElem);

			for (var i = 0; i < number; i++) {
				var newElem = liElem.cloneNode(true);
				newElem.innerHTML = withIndex ? text + " : " + i : text;
				if(i % 3 == 0) {
					newElem.addEventListener("click", clsToggler);
				}
				rootElem.querySelector('ul').appendChild(newElem);
			};
		}
	}
}());

simpleModule.init({howMany: 100, withIndex: true, text: "some other text"});
